﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//This is the colour of the image

namespace Fractal
{
    class HSB
    {

        public HSB()
        {
            
        }

        public Color RGB(float hue, float saturation, float brightness)
        {
            int r = 0, g = 0, b = 0;
            if (saturation == 0)
            {
                r = g = b = (int)(brightness * 255.0f + 0.5f);
            }
            else
            {
                float y = (hue - (float)Math.Floor(hue)) * 6.0f;
                float e = y - (float)Math.Floor(y);
                float n = brightness * (1.0f - saturation);
                float w = brightness * (1.0f - saturation * e);
                float v = brightness * (1.0f - (saturation * (1.0f - e)));
                switch ((int)y)
                {
                    case 0:
                        r = (int)(brightness * 255.0f + 0.5f);
                        g = (int)(v * 255.0f + 0.5f);
                        b = (int)(n * 255.0f + 0.5f);
                        break;
                    case 1:
                        r = (int)(w * 255.0f + 0.5f);
                        g = (int)(brightness * 255.0f + 0.5f);
                        b = (int)(n * 255.0f + 0.5f);
                        break;
                    case 2:
                        r = (int)(n * 255.0f + 0.5f);
                        g = (int)(brightness * 255.0f + 0.5f);
                        b = (int)(v * 255.0f + 0.5f);
                        break;
                    case 3:
                        r = (int)(n * 255.0f + 0.5f);
                        g = (int)(w * 255.0f + 0.5f);
                        b = (int)(brightness * 255.0f + 0.5f);
                        break;
                    case 4:
                        r = (int)(v * 255.0f + 0.5f);
                        g = (int)(n * 255.0f + 0.5f);
                        b = (int)(brightness * 255.0f + 0.5f);
                        break;
                    case 5:
                        r = (int)(brightness * 255.0f + 0.5f);
                        g = (int)(n * 255.0f + 0.5f);
                        b = (int)(w * 255.0f + 0.5f);
                        break;
                }
            }

            return Color.FromArgb(Convert.ToByte(255), Convert.ToByte(r), Convert.ToByte(g), Convert.ToByte(b));
        }
    }
}
