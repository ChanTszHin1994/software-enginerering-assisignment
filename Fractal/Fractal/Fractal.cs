﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Fractal
{
    public partial class Fractal : Form
    {
        private readonly int MAX = 256;      // max iterations
        private readonly double SX = -2.025; // start value real
        private readonly double SY = -1.125; // start value imaginary
        private readonly double EX = 0.6;    // end value real
        private readonly double EY = 1.125;  // end value imaginary
	    private static int x1, y1, xs, ys, xe, ye;
	    private static double xstart, ystart, xende, yende, xzoom, yzoom;
        private static bool action, rectangle, finished, down; //
	    private static double xy;
	    private Graphics g1;
        private Bitmap k = new Bitmap(640, 480);
        private Pen myPen = new Pen(Color.Red);
        private HSB HSBcol = new HSB();
        private System.Drawing.Printing.PrintDocument docToPrint =
        new System.Drawing.Printing.PrintDocument();
        public Fractal()
        {
            InitializeComponent();
        }
        private void initvalues() // reset start values
        {
            xstart = SX;
            ystart = SY;
            xende = EX;
            yende = EY;
            if ((float)((xende - xstart) / (yende - ystart)) != xy)
                xstart = xende - (yende - ystart) * (double)xy;

        }
        private void Fractal_Load(object sender, EventArgs e)// Loading the image
        {
            x1 = pictureBox1.Width;
            y1 = pictureBox1.Height;
            xy = (double)x1 / (double)y1;
            g1 = Graphics.FromImage(k);
            initvalues();
            xzoom = (xende - xstart) / (double)x1;
		    yzoom = (yende - ystart) / (double)y1;
            mandelbrot();
            pictureBox1.Image = k;  

        }

        private float pointcolour(double xwert, double ywert) // color value from 0.0 to 1.0 by iterations
        {
            double r = 0.0, i = 0.0, m = 0.0;
            int j = 0;
            while ((j < MAX) && (m < 4.0))
            {
                j++;
                m = r * r - i * i;
                i = 2.0 * r * i + ywert;
                r = m + xwert;
            }
            return (float)j / (float)MAX;
        }
        private void mandelbrot() // calculate all points
        {
            int x, y;
            float h, b = 0.0f;
            double alt = 0.0;

            action = false;
            for (x = 0; x < x1; x += 2)
                for (y = 0; y < y1; y++)
                {
                    h = pointcolour(xstart + xzoom * (double)x, ystart + yzoom * (double)y); //color value
                    if (h != alt)
                    {
                        b = 1.0f - h * h; // brightnes
                        ///djm added
                        ///HSBcol.fromHSB(h,0.8f,b); //convert hsb to rgb then make a Java Color
                        ///Color col = new Color(0,HSBcol.rChan,HSBcol.gChan,HSBcol.bChan);
                        ///g1.setColor(col);
                        //djm end
                        //djm added to convert to RGB from HSB

                        myPen.Color = HSBcol.RGB(h, 0.8f, b);//Convert the type of colour from HSB to RGB

                        alt = h;
                    }
                    g1.DrawLine(myPen, x, y, x + 1, y);
                }
            action = true;
        }

        

        private void Status_Click(object sender, EventArgs e)
        {

        }



        public void paint()
        {
            pictureBox1.Image = k;
        }

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            down = true;
            if (action)
            {
                xs = e.X;
                ys = e.Y;
            }
        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            int z, w;

            if (action)
            {
                xe = e.X;
                ye = e.Y;
                if (xs > xe)
                {
                    z = xs;
                    xs = xe;
                    xe = z;
                }
                if (ys > ye)
                {
                    z = ys;
                    ys = ye;
                    ye = z;
                }
                w = (xe - xs);
                z = (ye - ys);
                if ((w < 2) && (z < 2)) initvalues();
                else
                {
                    if (((float)w > (float)z * xy)) ye = (int)((float)ys + (float)w / xy);
                    else xe = (int)((float)xs + (float)z * xy);
                    xende = xstart + xzoom * (double)xe;
                    yende = ystart + yzoom * (double)ye;
                    xstart += xzoom * (double)xs;
                    ystart += yzoom * (double)ys;
                }
                xzoom = (xende - xstart) / (double)x1;
                yzoom = (yende - ystart) / (double)y1;
                mandelbrot();
                rectangle = false;
                paint();
            }
            down = false;
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if (down && action)
            {

                xe = e.X;
                ye = e.Y;
                rectangle = true;
                Bitmap myMap = new Bitmap(k);
                Graphics g2 = Graphics.FromImage(myMap);
                myPen.Color = Color.Black;
                if (xs < xe)
                {
                    if (ys < ye) g2.DrawRectangle(myPen, xs, ys, (xe - xs), (ye - ys));
                    else g2.DrawRectangle(myPen, xs, ye, (xe - xs), (ys - ye));
                }
                else
                {
                    if (ys < ye) g2.DrawRectangle(myPen, xe, ys, (xs - xe), (ye - ys));
                    else g2.DrawRectangle(myPen, xe, ye, (xs - xe), (ys - ye));
                }

                pictureBox1.Image = myMap;

            }
        }

        private void statusStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        

        private void timer1_Tick(object sender, EventArgs e)
        {

        }

        private void saveFileDialog1_HelpRequest(object sender, EventArgs e)
        {

        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void fileToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string ChosenFile = "";
            save.FileName = "";
            save.Filter = "png(*.png)|*.png|jpg(*.jpg)|*.jpg|ALL Files(*.*)|*.*";
            save.RestoreDirectory = true;
            ChosenFile = save.FileName;
            ChosenFile = save.FileName;
            if (save.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                k.Save(save.FileName, System.Drawing.Imaging.ImageFormat.Png);
            }
            save.RestoreDirectory = true;
        }

        private void helpToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("You can zoom in the image and right click to undo it!","Fractal");
        }

        private void save_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void aboutUsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Name: Chan Tsz Hin", "Fractal");
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void printToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        

        
       

        
        

        

        

       

       
    }
}
